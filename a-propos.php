<?php
session_start();
ob_start();
?>
<section>
    <div class="barber">
        <div class="barber__titre">
            <h1>Notre équipe</h1>
        </div>
        <div class="grid-container fluid">
            <div class="grid-x align-justify">
                <div class="cell small-6 large-2">
                    <div class="barber__pers">
                        <img src="./ressources/images/barber/barber1.jpg" alt="" > 
                        <p>George</p>
                    </div>
                </div>
                <div class="cell small-6  large-2">
                    <div class="barber__pers">
                        <img src="./ressources/images/barber/barber2.jpg" alt="" > 
                        <p>Monica</p>
                    </div>
                </div>
                <div class="cell small-6  large-2">
                    <div class="barber__pers">
                        <img src="./ressources/images/barber/barber3.jpg" alt="" > 
                        <p>Arnaud</p>
                    </div>
                </div>
                <div class="cell small-6  large-2">
                    <div class="barber__pers">
                        <img src="./ressources/images/barber/barber1.jpg" alt="" > 
                        <p>George</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bloc_apropos">
        <div class="grid-container fluid">
            <div class="grid-x align-center">
                <div class="cell large-4">
                    <div class="bloc_apropos__contenu">
                        <h3>The Barber Company</h3>
                        <p>Barbershop à Clermont ferrand</p>
                        <p>Vous Recherchez un Barbershop à Clermont ferrand ?</p>
                        <p>Optez pour notre Salon de coiffure pour homme The Barber Company. Salon Barbier Ouvert 6J/7 nos coiffeurs pour homme et enfant sont là pour prendre Soin de vos Barbes, Cheveux & Moustaches. Ambiance Décontractée.</p>
                        <p>The Barber Company à Clermont ferrand vous propose un service haut de gamme à des prix abordables sans vous ruinez.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
$contenu=ob_get_clean();
require_once './template/header.php';
?>
<?php
session_start();
if (!isset($_SESSION['statut']) OR $_SESSION['statut'] !== '1') 
{
    header("Location: index.php");
    exit;
}
ob_start();
?>
<h1>Edit</h1>
<button class="button"><a href="./edit_prestation.php">Prestation</a></button>
<button class="button"><a href="./edit_photo.php">Galerie</a></button>
<div id="donnees">

</div>

<?php
$contenu=ob_get_clean();
require_once './template/header.php';

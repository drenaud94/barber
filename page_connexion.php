<?php
ob_start();
include './data/connexion.php';
if (isset($erreur)) 
{
    echo $erreur;
}
?>
<section class="connexion">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell">
                <div class="bloc_connexion">
                    <div class="grid-container">
                        <div class="grid-x">
                            <div class="cell large-6">
                                <div class="bloc_image_connexion">
                                    <div class="image_connexion">
                                        <a href='https://fr.pngtree.com/so/plage'>
                                            <img src="./ressources/images/autres/beard_mustache.png" alt="plage png de fr.pngtree.com" title="plage png de fr.pngtree.com">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="cell large-6">
                                <div class="bloc_form">
                                    <h1>Connexion</h1>
                                    <form action="" method="post">
                                        <div class="div_input">
                                            <input class="input_co" type="text" name="login" placeholder="Identifiant">
                                            <div class="icone_form">
                                                <i class="fas fa-id-badge"></i>
                                            </div>
                                        </div>
                                        <div class="div_input">
                                            <input class="input_co" type="password" name="password" id="" placeholder="Password">
                                            <div class="icone_form">
                                                <i class="fas fa-lock"></i>
                                            </div>
                                        </div>
                                        <button type="submit" name='connexion' class="btn_form">Login</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$contenu= ob_get_clean();
require_once './template/header.php';
<?php
session_start();
include './data/info_presta.php';

function Prestation($type)
{
    $length=count($type);
    $l=0;
    while ($l<$length) 
    {
        $nom=$type[$l]['nom'];
        $desc=$type[$l]['desc'];
        $prix=$type[$l]['prix'];
        $l++;
    echo('
        <div class="bloc_prestation__bloc">
            <div class="bloc_prestation__contenu">
                <p>'.$nom.'</p>
                <p class="bloc_prestation__prix">'.$prix.'€</p>
            </div>
            <i>'.$desc.'</i>
        </div>
        ');
    }
}

ob_start();
?>

<section class="section_prestation">
    <div class="titre_prestation">
        <h1>Prestation</h1>
        <p>De la coupe de cheveux à la barbe, on vous offre un large éventail de prestations afin de répondre aux demandes les plus exigeantes, le tout dans un environnement agréable, détendu et élégant.</p>
    </div>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell">
                <div class="bloc_prestation">
                    <h3>Barbe</h3>
                    <?php
                    Prestation($barbe);
                    ?>
                </div>
            </div>
            <div class="cell">
            <div class="bloc_prestation">
                    <h3>Cheveux</h3>
                    <?php
                    Prestation($cheveux);
                    ?>
                </div>
            </div>
            <div class="cell">
            <div class="bloc_prestation">
                    <h3>Soin</h3>
                    <?php
                    Prestation($soins);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$contenu=ob_get_clean();
require_once './template/header.php';
?>
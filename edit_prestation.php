<?php
session_start();
if (!isset($_SESSION['statut']) OR $_SESSION['statut'] !== '1') 
{
    header("Location: index.php");
    exit;
}
ob_start();
?>
<div class="grid-container gestion">
    <div class="grid-x align-center">
        <div class="cell gestion__titre" >
            <h1>Gestion des Prestations</h1>
        </div>
        <div class="cell large-4 gestion__btn">
            <div>
                <button onclick="$('#contenu').load('./update_prestation.php')" class="button">Modifier</button>
                <button onclick="$('#contenu').load('./add_prestation.php')" class="button">Ajouter</button>
            </div>
        </div>
    </div>
</div>

    <div class="grid-container">
        <div class="grid-x align-center" id="contenu">

        </div>
    </div>


<?php
$contenu=ob_get_clean();
require_once './template/header.php';

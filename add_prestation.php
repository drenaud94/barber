<?php
include './config/configuration.php';
?>
<div class="cell large-5">
    <div class="form_presta">
        <form action="./data/add.php" method="post">
            Type: <select name="id" id="type">
            <?php
                $data=$bdd->query('SELECT * FROM type');
                while($types=$data->fetch())
                {
                    ?>
                    <option value="<?=$types['id'];?>"><?=$types['nom'];?></option>
                    <?php
                }
            ?>
            </select>
            Nom:<input type="text" name="nom" placeholder="Nom">
            Description:<textarea name="desc"  cols="30" rows="4" placeholder="Description"></textarea>
            Prix:<input type="number" name="prix" placeholder="....€">
            <button type="submit" class="button">Ajouter</button>
        </form>
    </div>
</div>
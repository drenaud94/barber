<?php
session_start();
include('./config/configuration.php');
ob_start();

?>
<section class="galerie">
    <div class="galerie__titre">
        <h1>Galerie</h1>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-margin-x align-center" id="galerie_photo">

        </div>
    </div>
</section>
<script>
$(function(){
    $.ajax({
        url:'./data/info_photo.php',
        dataType:'json',
        success: function(data){
            
            var longueur = data.length
            var l=0;
            //boucle pour ajouter les résultats obtenus
            while (l<longueur) 
            {
                $('#galerie_photo').append('<div class="galerie__image"><div class="cell small-6 large-3"><img src="./ressources/images/modeles/'+data[l]["nom"]+'" alt=""></div></div>')
                var l= l+1
            }                    
        },              
    });
})
</script>
<?php
$contenu=ob_get_clean();
require_once './template/header.php';

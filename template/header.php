<?php
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Compressed CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
    <link rel="stylesheet" href="./ressources/Font_Awesome/css/all.css">
    <link rel="stylesheet" href="ressources/css/style.css">
    <script src="./ressources/js/jquery.js"></script>
    <!-- Compressed JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/js/foundation.min.js" integrity="sha256-pRF3zifJRA9jXGv++b06qwtSqX1byFQOLjqa2PTEb2o=" crossorigin="anonymous"></script>
</head>
<body>    
    <header>
        <div class="grid-container fluid navigation">
            <div class="grid-x bloc">
                <div class="cell small-4 large-2">
                    <a href="./index.php">
                        <img src="./ressources/images/Logo-2.jpg" alt="">
                    </a>
                </div>
                <div class="cell small-8 large-8">
                <div class="js_menuBtn"></div>
                    <ul class="menu align-right dropdown menu" data-dropdown-menu>
                        <li><a href="./index.php">Accueil</a></li>
                        <li><a href="./a-propos.php">A propos</a></li>
                        <li><a href="./prestation.php">Prestation</a></li>
                        <li><a href="./contact.php">Contact</a></li>
                        <?php
                            if (isset($_SESSION['statut']) AND $_SESSION['statut'] =='1') 
                            {
                                ?>
                                <li class="affiche_menu"> <a href="" >Editer</a>
                                    <ul class="menu-deroulant">
                                        <li><a href="./edit_prestation.php">Prestation</a></li>
                                        <li><a href="./edit_photo.php">Galerie</a></li>
                                    </ul>
                                </li>
                                <?php
                                // echo ('<li><a href="./edit.php">Editer</a></li>');
                            }
                            if (isset($_SESSION['login'])) 
                            {
                                echo ('<li><a href="./data/deconnexion.php">Deconnexion</a></li>');
                            }
                            else
                            {
                                echo ('<li><a href="./page_connexion.php">Connexion</a></li>');
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <?php echo $contenu;?>
    <footer>
    <div class="footer">
        <div class="grid-container">
            <div class="grid-x align-center grid-margin-x">
            <div class="cell small-6 large-3">
                <div>
                    <h3 class="footer__title">Horaire d'ouverture :</h3>
                    <p class="footer__text">Du lundi au samedi : 10h à 19h <br>Dimanche: fermé</p>
                </div>
            </div>
            <div class="cell small-6 large-3">
                <h3 class="footer__title">Infos salon:</h3>
                <p class="footer__text">10 Rue Gilbert Romme, 63000 Clermont-Ferrand<br>
                    04 73 01 01 01<br>
                    atelierbarber@stop.com</p>
                <div></div>
            </div>
            <div class="cell small-6 large-2">
                <h3 class="footer__title">Navigation</h3>
                <ul class="footer__menu">
                    <li><a href="./index.php">Accueil</a></li>
                    <li><a href="./a-propos.php">A propos</a></li>
                    <li><a href="./prestation.php">Prestation</a></li>
                    <li><a href="./contact.php">Contact</a></li>
                </ul>
            </div>
            <div class="cell small-6 large-2">
            <a href="./contact.php"><button class="button">Prendre RDV</button></a>
            </div>
            </div>
        </div>
    </div>
</footer>
<script>
    $(".js_menuBtn").on("click",function()
    {
        $(".js_menuBtn").toggleClass("js-menuOpen");
        $(".menu").toggleClass("js-open");
    })
</script>
</body>
</html>
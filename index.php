<?php
session_start();
include './config/configuration.php';
ob_start();
?>
<section class="accueil">
    <div class="accueil_ban">
    </div>
    <div class="accueil__bloc_texte">
        <div class="grid-container fluid">
            <div class="grid-x align-justify">
                <div class="cell small-5 medium-5 large-4">
                    <div class="accueil__texte">
                        <div class="contenu-accueil">
                            <h1>Découvrez notre savoir-faire...</h1>
                        </div>
                        <div>
                            <button class="button bouton_accueil"><a href="./galerie.php">Galerie</a></button>
                        </div>
                    </div>
                </div>
                <div class="cell small-5 medium-5 large-4">
                    <div class="accueil__texte">
                        <div class="contenu-accueil">
                            <h1>...Et partez à l'abordage</h1>
                        </div>
                        <div>
                            <button class="button bouton_accueil"><a href="./prestation.php">Prestation</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="accueil__div-icone">
        <a href="#a_propos"><i class="fas fa-angle-down fa-5x" id="icone_accueil"></i></a>
    </div>
    <!-- <div class="mustache">
        <img src="./ressources/images/autres/mustache.png" alt="">
    </div> -->
</section>
<section class="a_propos" id="a_propos">
    <div class="grid-container fluid">
        <div class="grid-x align-center">
            <div class="cell large-5 a_propos_image">
                <img src="./ressources/images/photos/siège.jpg" alt="" class>
                <img src="./ressources/images/photos/siège.jpg" alt="">
            </div>
            <div class="cell large-3">
                <h2>L'atelier Barber ne laisse rien au hasard !</h2>
                <p>Accompagné de 4 barbiers des plus minutieux, toute l'équipe met la maîtrise des méthodes traditionnelles au centre des priorités. Nos barbiers experts se donnent pour mission d’offrir à la clientèle des prestations étudiées au cas-par-cas, de manière à respecter l’implantation du cheveu, et d’ainsi élaborer une coupe de cheveux qui perdure, le tout dans les règles de l’art de la coiffure authentique.</p>
                <button class="button"> <a href="./a-propos.php">En savoir plus</a></button>
            </div>
        </div>
    </div>
</section>

<section class="service">

    <div class="service_banniere">
        <div class="service_titre">
            <h1>Nos services</h1>
        </div>
    </div>

    <div class="service_desc">
        <div class="grid-container">
            <div class="grid-x align-center">
                <div class="cell">
                    <div class="service_contenu">

                        <div class="grid-container">
                            <div class="grid-x grid-margin-x align-center">
                                <div class="cell large-6">
                                    <?php
                                    $data=$bdd->query('SELECT * FROM prestation LIMIT 3');
                                        while ($donnees=$data->fetch()) 
                                        {
                                            $nom=$donnees['nom'];
                                            $prix=$donnees['prix'];

                                            ?>
                                            <div class="single_service">
                                                <div class="single_service__item">
                                                    <img src="./ressources/images/photos/coupe.jpg" alt="">
                                                    <p><?=$nom;?></p>
                                                </div>
                                                <p>................. <?=$prix;?>€</p>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                                <div class="cell large-6">
                                <?php
                                    $data=$bdd->query('SELECT * FROM prestation LIMIT 3 OFFSET 3');
                                        while ($donnees=$data->fetch()) 
                                        {
                                            $nom=$donnees['nom'];
                                            $prix=$donnees['prix'];

                                            ?>
                                            <div class="single_service">
                                                <div class="single_service__item">
                                                    <img src="./ressources/images/photos/coupe.jpg" alt="">
                                                    <p><?=$nom;?></p>
                                                </div>
                                                <p>................. <?=$prix;?>€</p>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <script>
$(function(){
    $('#icone_accueil').click(function(){
        window.scrollTo({
            top:1000,
            left:0,
            behavior:"smooth"
        })
    })
})
</script> -->
<?php
$contenu= ob_get_clean();
require_once './template/header.php';

<?php
session_start();
if (!isset($_SESSION['statut']) OR $_SESSION['statut'] !== '1') 
{
    header("Location: index.php");
    exit;
}

ob_start();
?>
<div>
    <h1></h1>
    <div class="grid-container">
        <div class="grid-x grid-margin-x" id="edit_photo">

            </div>
        </div>
    </div>
</div>
<div class="grid-container">
    <div class="grid-x align-center" id="contenu_photo">
    <button class="button" onclick="Ajouter()">Ajouter</button>
    </div>
</div>

<script>
function Ajouter()
{
    document.getElementById('contenu_photo').innerHTML='<div class="cell large-6"><div class="form_presta"><form action="./data/add_photo.php" method="post" enctype="multipart/form-data"> <input type="file" name="img"> <button type="submit" class="button">Envoyer</button> </form> </div></div>'
}
function Delete(id)
    {
        if (confirm("Etes-vous sur de vouloir supprimer cette photo?"))
        {
            window.location.replace("./data/delete_photo.php?id="+id+"")
        }
    }
$(function(){
    
    $.ajax({
        url:'./data/info_photo.php',
        dataType:'json',
        success: function(data){
            
            var longueur = data.length
            var l=0;
            //boucle pour ajouter les résultats obtenus
            while (l<longueur) 
            {
                $('#edit_photo').append('<div class="cell small-6 large-3"><img src="./ressources/images/modeles/'+data[l]["nom"]+'" alt=""><button class="button" onclick="Delete('+data[l]['id']+')">Supprimer</button></div>')
                var l= l+1
            }                    
        },              
    });

})
             
</script>

<?php
$contenu=ob_get_clean();
require_once './template/header.php';
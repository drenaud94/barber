<?php
include './config/configuration.php';
?>
<div class="bloc_select">
    <h1>Séléctionner le type de prestation à éditer :</h1>

    <select name="" id="option" class="selecteur_presta">
        <option value=""></option>
        <?php
            $data=$bdd->query('SELECT * FROM type');
            while($types=$data->fetch())
            {
                ?>
                <option value="<?=$types['id'];?>"><?=$types['nom'];?></option>
                <?php
            }
        ?>
    </select>
</div>

<div class="grid-container">
    <div class="grid-x grid-margin-x" id="content">
    </div>
</div>


<script>
    function Delete(id)
    {
        if (confirm("Etes-vous sur de vouloir supprimer cette prestation?"))
        {
            window.location.replace("./data/delete_prestation.php?id="+id+"")
        }
    }
    $('#option').change(function(){
        //Récupère la valeur du select (id du type de prestation)
        var type = $('#option option:selected').val();
        $('#content').html('')
        var test = $.isNumeric(type)
        // si la valeur du select est bien un chiffre
        if(test == true) 
        {
            //on récupère les données de la page qui se charge de faire la requête en fonction du type (id) sélectionné
            $.ajax({
                url:'./data/info_presta.php?type='+type,
                dataType:'json',
                success: function(data){
                    
                    var longueur = data.length
                    var l=0;
                    //boucle pour ajouter les résultats obtenus
                    while (l<longueur) 
                    {
                        $('#content').append('<div class="cell large-6"><div class="form_presta"><form action="./data/update.php" method="post">Nom:<input type="text" value="'+data[l]['nom']+'" name="nom">Description:<textarea name="desc" cols="30" rows="4">'+data[l]['desc']+'</textarea>Prix:<input type="number" value="'+data[l]['prix']+'" name="prix"><input type="number" name="id" value="'+data[l]['id']+'" class="input_cache"><button class="button" type="submit">Valider</button> </form><button onclick="Delete('+data[l]['id']+')">Supprimer<button><div></div>')
                        var l= l+1
                    }                    
                },              
            });           
        }
    })
</script>

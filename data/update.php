<?php
include '../config/configuration.php';

$nom=htmlspecialchars($_POST['nom']);
$id=intval($_POST['id']);
$desc=htmlspecialchars($_POST['desc']);
$prix=intval($_POST['prix']);

$update=$bdd->prepare('UPDATE prestation SET nom=:nom, description=:desc, prix=:prix WHERE id=:id');
$update->bindParam(':nom', $nom);
$update->bindParam(':desc', $desc);
$update->bindParam(':prix', $prix);
$update->bindParam(':id', $id);
$update->execute();
?>

<script>
    alert("Les informations ont été mises à jour"); 
    window.location.replace("../edit_prestation.php")

</script>

<?php
session_start();
include '../config/configuration.php';
if (isset($_FILES['img'])) 
{
    $nom=basename($_FILES['img']['name']);;
    $dossier = './ressources/images/modeles/';
    $extensions = array('.png', '.gif', '.jpg', '.jpeg');
    $extension = strrchr($_FILES['img']['name'], '.');

    //vérifications extensions
    if(!in_array($extension, $extensions))
    //Si l'extension n'est pas dans le tableau
    {
        $erreur = 'Vous devez uploader un fichier de type png, gif, jpg ou jpeg...';
        header('Refresh:2;edit_photo.php');
    }

    //S'il n'y a pas d'erreur on upload
    if(!isset($erreur))
    {
        //On formate le nom du fichier ici...
        $fichier = strtr($nom,
            'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
            'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

        if(move_uploaded_file($_FILES['img']['tmp_name'], $dossier . $fichier))
        //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
        {
            //Vérifier que l'image n'existe pas déjà
            $verif = $bdd->prepare('SELECT nom FROM galerie WHERE nom= :nom');
            $verif->bindParam(':nom',$fichier);
            $verif->execute();
            $exist=$verif->rowCount();
            //si l'image n'existe pas
            if ($exist==0) 
            {
                $req = $bdd->prepare('INSERT INTO galerie(nom, id_user) VALUES(:fichier, :user)'); 
                $req->bindParam(':fichier',$fichier);
                $req->bindParam(':user',$_SESSION['id_user']);
                $req->execute();
                header('location:edit_photo.php');
            }
            else
            {
                echo ('Cette image a déjà été enregistrée!');
                header('Refresh:2;edit_photo.php');
            }
        }
        else
        {
            
            echo 'Echec de l\'upload !';
            header('Refresh:2;edit_photo.php');
        }
    }
    else
    {
        echo $erreur;
        header('Refresh:2;edit_photo.php');
    }
}
?>

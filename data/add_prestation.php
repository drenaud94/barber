<?php

include '../config/configuration.php';
if (isset($_POST['id']) AND isset($_POST['nom']) AND isset($_POST['desc']) AND isset($_POST['prix'])) 
{
    $nom=htmlspecialchars($_POST['nom']);
    $id=intval($_POST['id']);
    $desc=htmlspecialchars($_POST['desc']);
    $prix=intval($_POST['prix']);

    $insert=$bdd->prepare('INSERT INTO prestation (nom, description, prix, id_user, id_type) VALUES (:nom, :desc, :prix, :user, :type)');
    $insert->bindParam(':nom', $nom);
    $insert->bindParam(':desc', $desc);
    $insert->bindParam(':prix', $prix);
    $insert->bindParam(':user', $_SESSION['id_user']);
    $insert->bindParam(':type', $id);
    $insert->execute();
}
header('location:../edit_prestation');
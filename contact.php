<?php
session_start();
ob_start();
?>
<section id="contact">
<div class="grid-container" style="padding: 5% 0;">
    <div class="grid-x grid-margin-x grid-margin-y">
        <div class="cell large-6">
            <div style="text-align: center;background: white;">
                <h1>Contactez-nous</h1>
                <form action="./data/mail.php" method="post" style="padding:0 10%;">
                    <input type="text" name="msg" id="" placeholder="Nom">
                    <input type="email" name="email" id="" placeholder="Email">
                    <textarea name="" id="" cols="15" rows="10" placeholder="Message"></textarea>
                    <button type="submit" class="button">Send Your Message</button>
                </form>
            </div>
        </div>
        <div class="cell large-6" style="background-color: white; text-align:center; padding:5%">
            <div>
                <h2>L'atelier Barber</h2>
            </div>
            <div style="height:35vh">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2783.1820884861586!2d3.098913815836232!3d45.76754092117158!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f71bf4d7148817%3A0x4129505ec6079f0e!2s10%20Rue%20Gilbert%20Romme%2C%2063000%20Clermont-Ferrand!5e0!3m2!1sfr!2sfr!4v1619602435273!5m2!1sfr!2sfr" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <div style="padding: 5% 0 0;">
                <h3>Téléphone</h3>
                <p>04 73 01 01 01</p>
            </div>
        </div>
    </div>
</div>
    
</section>

<?php
$contenu=ob_get_clean();
require_once './template/header.php';
?>